Quicklook S2 is a Python script to generate a visible image from ESA Sentinel-2 tile data.

To see help on the parameters, run:  
  
```
#!python

python quicklook_S2.py --help
```
  
This shows the available options.  
  

```
#!python

usage: quicklook_S2.py [-h] [-f FILENAME] [-g GAIN]

Create a quicklook image from ESA Sentinel-2 tile data.

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        Sentinel-2 tile ZIP-file name
  -p PROJ4STR, --projection PROJ4STR
                        Map projection as PROJ4 string
  -g GAIN, --gain GAIN  Brightness limits for RGB channels [default:
                        "5500,5500,5500"]
```

The program needs the name of a Sentinel-2 tile subset ZIP-file.  These can be obtained by running the [Check ESA SciHub program](https://bitbucket.org/Polarnix/check-esa-scihub) with, for example, options like the following:  

```
#!python

python check_ESA_SciHub.py -s Sentinel-2 -a 33WXT -n "2,3,4" -d 1
```
  
In this example, all data covering tile 33XWT (Troms�) for the past 3 days will be downloaded.  Quicklook S2 should then be run on the ZIP-files resulting from this.
  
Apart from the filename, the only other optional user parameters that can be supplied are to:

  * Specify the gain (brightness) for the 3 channels.  This is done by supplying a string, and which by default is set to "5500,5500,5500".

  * Supply a map projection for the output GeoTIFF image. The default is set to "auto", which is the UTM map projection the Sentinel-2 image is supplied in.  Other shortcut options are "north" and "south" for North and South Polar Stereographic respectively. Otherwise the user should supply a valid PROJ4 map projection string.
  
The software generates a full resolution GeoTIFF image with JPEG-compression, and internal overviews/pyramids.  The file size is around 30 MB, much reduced from the size of the original data.

**Citing this software**  
This software has DOI: 10.5281/zenodo.159452 on Zenodo.  
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.159452.svg)](https://doi.org/10.5281/zenodo.159452)
