#!/usr/bin/python

# Name:          quicklook_S2.py
# Purpose:       Script to generate a high resolution quick look (but with small file size) from
#                Sentinel-2 visible bands.  Takes as input the tile subset zip-files generated by
#                check_ESA_SciHub.py (https://bitbucket.org/Polarnix/check-esa-scihub).
#
#                Please note:
#                Needs a recent version of GDAL installed, tested on version 2.1.1. This can be
#                installed as a separate installation to the GDAL version supplied with Ubuntu,
#                you just need to set LD_LIBRARY_PATH and PYTHONPATH before calling this script by,
#                for example:
#                  export LD_LIBRARY_PATH=/usr/local/gdal-2.1.1/lib/
#                  export PYTHONPATH=/usr/local/gdal-2.1.1/lib/python2.7/site-packages
#
# Author(s):     Nick Hughes
# Created:       2016-x-5
# Modifications: 2017-v-25  - Add map projection selection capability.
#                2018-v-9   - Changes to improve reliability on GDAL 2.2.4.
#                2020-vi-1  - Update for Python 3.
# Copyright:     (c) Norwegian Meteorological Institute, 2016
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys
import zipfile
import argparse

import osgeo.gdal as gdal
import osgeo.osr as osr

# import imp

import numpy as N


# Quicklook routine for Sentinel-2
def quicklook_S2( s2fn, mapproj, gain ):
    
    # See if it possible to open Sentinel-2 zip-file, then get list of the files inside
    try:
        zipf = zipfile.ZipFile( s2fn, 'r' )
    except:
        print ("Unable to open ZIP-file.")
        return 'None'
    tmpflist = zipf.namelist()
    zipf.close()

    # Filter flist for JP2-files only
    flist = []
    for fname in tmpflist:
        if fname[-4:] == '.jp2':
            flist.append(fname)
    # print flist

    # RGB channel numbers 
    channels = [ 4, 3, 2 ]

    # Check we have all the RGB channels present
    chnfnlist = []
    for chnno in channels:
        teststr = ("_B%02d" % chnno)
        for fname in flist:
            if fname.find(teststr) > -1:
                chnfnlist.append( ("/vsizip/%s/%s" % (s2fn,fname) ) )
    if len(chnfnlist) < 3:
        print ("All 3 channels not available.")
        return 'None'

    # Get root filename
    path, fname = os.path.split( s2fn )
    rootfn = fname[:-4]

    # Turn off error messages and use exceptions
    gdal.UseExceptions()
    gdal.PushErrorHandler('CPLQuietErrorHandler')

    # Output GDAL driver
    tifdrv = gdal.GetDriverByName("GTiff")

    # Set GDAL_CACHEMAX
    gdal.SetConfigOption( 'GDAL_CACHEMAX', "64" )
    gdal.SetConfigOption( 'VIS_CACHE', "TRUE" )
    gdal.SetConfigOption( 'VIS_CACHE_SIZE', "67108864" )

    # Output projection
    out_srs = osr.SpatialReference()
    # print mapproj
    if mapproj == 'auto':
        # Get image information and use that as output map projection (S2 images are already UTM)
        in_ds = gdal.Open( chnfnlist[0], 0)
        in_srs_wkt = in_ds.GetProjectionRef()
        out_srs.ImportFromWkt(in_srs_wkt)
        in_ds = None
    elif mapproj == 'north':
        projstr = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
        out_srs.ImportFromProj4( projstr )
    elif mapproj == 'south':
        projstr = '+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'
        out_srs.ImportFromProj4( projstr )
    else:
        try:
            out_srs.ImportFromProj4( mapproj )
        except:
            print ("Problem with map projection PROJ4 string \"%s\"" % projstr)
            sys.exit()
    out_srs_wkt = out_srs.ExportToWkt()
    # print out_srs_wkt

    # Resampling parameters
    resampling = gdal.GRA_Bilinear
    error_threshold = 0.125

    # Loop through channels
    idx = 0
    for inpfn in chnfnlist:

        # Open the input JP2-file (from within the zip-file) and get the georeferencing information
        in_ds = gdal.Open( inpfn, 0)
        in_srs_wkt = in_ds.GetProjectionRef()
        in_srs = osr.SpatialReference()
        in_srs.ImportFromWkt(in_srs_wkt)

        # Create a virtual raster so that we can get the image size
        chn_ds = gdal.AutoCreateWarpedVRT( in_ds, in_srs_wkt, out_srs_wkt, resampling, error_threshold )

        # Carry out the reprojection/resampling
        #   NB: This seems to generate errors due to dirty data blocks, but output seems OK so ignore!
        try:
            res = gdal.ReprojectImage( in_ds, chn_ds, None, None, resampling, 0.0, error_threshold )
        except:
            errmsg = gdal.GetLastErrorMsg()
            # print errmsg

        # If first channel, create a new output GeoTIFF
        if idx == 0:
            out_geotrans = chn_ds.GetGeoTransform()
            cols = chn_ds.RasterXSize
            rows = chn_ds.RasterYSize
            newfn = ("%s.tif" % rootfn)
            out_ds = tifdrv.Create( newfn, cols, rows, 3, gdal.GDT_Byte, \
                options=[ "COMPRESS=JPEG", "JPEG_QUALITY=75" ] )
            out_ds.SetGeoTransform( out_geotrans )
            out_ds.SetProjection( out_srs_wkt )

        # Load data so that we can rescale it to 8-bit
        in_band = chn_ds.GetRasterBand(1)
        data = in_band.ReadAsArray()
        data = N.array( data, dtype=N.float32 )
        data = data * (256.0 / float(gain[idx]))
        data = N.array( data, dtype=N.float32 )
        over_idx = N.nonzero( data > 255 )
        data[over_idx] = 255.0

        # Write data to output file band
        out_band = out_ds.GetRasterBand(idx+1)
        out_band.WriteArray( data.astype(N.uint8) )
        out_band.FlushCache()
        out_band.SetNoDataValue(0)

        # Close the channel input virtual file
        chn_ds = None
        in_ds = None

        # Increment the channel index
        idx = idx + 1

    # Build overviews
    gdal.SetConfigOption( 'COMPRESS_OVERVIEW', 'JPEG' )
    gdal.SetConfigOption( 'PHOTOMETRIC_OVERVIEW', 'YCBCR' )
    out_ds.BuildOverviews( 'AVERAGE', overviewlist=[2,4,8,16,32,64,128,256])

    # Close the input and output files
    out_ds = None
        
    return newfn


# Main program routine
if __name__ == '__main__':

    # Get user options
    parser = argparse.ArgumentParser(description='Create a quicklook image from ESA Sentinel-2 tile data.')
    parser.add_argument("-f", "--filename", help="Sentinel-2 tile ZIP-file name", type=str, \
        default='None' )
    parser.add_argument("-p", "--projection", help="Map projection as PROJ4 string", type=str, \
        default='auto' )
    parser.add_argument("-g", "--gain", help="Brightness limits for RGB channels [default: %(default)s]", type=str, \
        default='\"5500,5500,5500\"' )
    args = parser.parse_args()
    # print args

    # Extract arguments and further check arguments
    s2fn = args.filename
    if s2fn == 'None':
        print ("S2 tile ZIP-file name required.")
        sys.exit()
    mapproj = args.projection
    gainstr = args.gain
    bits = (gainstr.replace('\"','')).split(',')
    gain = []
    for bit in bits:
        try:
            gainval = int(bit)
        except:
            print ("Problem decoding gain value information")
            sys.exit()
        else:
            gain.append(gainval)
    if len(gain) < 3:
        print ("Too few gain values, you need to supply 3 for the red, green and blue channels.")
        sys.exit()
    elif len(gain) > 3:
        print ("Too many gain values, you only need to supply 3 for the red, green and blue channels.")
        sys.exit()
        
    # Call the quicklook generating routine
    newfn = quicklook_S2( s2fn, mapproj, gain )
    print (newfn)
